from django import forms
from django.forms import ModelForm
from .models import ShoppingItem, ShoppingList

class ItemForm(forms.ModelForm):

    class Meta:
        model = ShoppingItem
        fields = '__all__'

class ListForm(forms.ModelForm):

    class Meta:
        model = ShoppingList
        fields = '__all__'
