from django.db import models

# Create your models here.

class ShoppingList(models.Model):
    name = models.CharField(max_length=100, null=False)

    def __str__(self):
        return f"{self.name}"

class ShoppingItem(models.Model):
    name = models.CharField(max_length=100, null=False)
    quantity = models.IntegerField(default=0, null=False)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00, null=False)
    done = models.BooleanField(default=False, null=False)
    shoppinglist = models.ForeignKey(ShoppingList, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.quantity} {self.price} {self.done} {self.shoppinglist}"

