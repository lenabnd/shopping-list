"""shoppingproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views

app_name = 'shoppingapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:shoppinglist_id>/', views.detail, name="detail"),
    path('new_list', views.new_list, name="new_list"),
    path('new_item', views.new_item, name="new_item"),
    path('new_item_anlegen', views.new_item_anlegen, name="new_item_anlegen"),
    path('update_item/<int:shoppingitem_id>/', views.updateItem, name="update_item"),
    path('update_list/<int:shoppinglist_id>/', views.updateList, name='update_list'),
    path('delete_item/<int:shoppingitem_id>/', views.deleteItem, name='delete_item'),
    path('delete_list/<int:shoppinglist_id>/', views.deleteList, name='delete_list'),
]